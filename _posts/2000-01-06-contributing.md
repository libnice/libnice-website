---
title: "contributing"
bg: turquoise
color: white
fa-icon: code-fork
---

Support requests and general discussions should on the [mailing list](https://lists.freedesktop.org/mailman/listinfo/nice), a [full archive](https://lists.freedesktop.org/archives/nice/) is available.

Contributions are welcome! Please reports bugs  using [freedesktop.org GitLab](https://gitlab.freedesktop.org/libnice/libnice/issues/).

Patches can also be submitted as merge requests on our [GitLab](https://gitlab.freedesktop.org/libnice/libnice/).

Merge requests are NOT accepted on the [GitHub](https://github.com/libnice/libnice) or [GitLab.com](https://gitlab.com/libnice/libnice) mirrors.


### Code of Conduct

libnice is hosted by freedesktop.org and as such applies its [Code of Conduct](https://www.freedesktop.org/wiki/CodeOfConduct/). This applies to all of our forums, including GitLab and the mailing list. Please be excellent to each other.
