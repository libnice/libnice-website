---
title: "docs"
bg: purple
color: white
fa-icon: book
---

## Reference manual

The generated documentation for the latest released version is [available online](libnice/).

## GStreamer

libnice has been designed to nice integrate with GStreamer, two GStreamer element are provided `nicesrc` and `nicesink` to help with that.

libnice works with both GStreamer 0.10 and GStreamer 1.x.

Steps to use:

- Create a `NiceAgent`
- Create a stream with `nice_agent_add_stream()`
- Create a `nicesrc` GStreamer element for each component
- Set the `NiceAgent`, `stream_id`  and `component_id` on each `nicesrc`
- Put the `nicesrc` elements inside a `GstPipeline` and connect them to the elements that will receive the packets, most likely rtpbin
-  Set the `GstPipeline` to the `PLAYING` state
- libnice can now receive packets.
- Use the `nice_agent_gather_candidates()` API to gather candidates
- Use the libnice APIs to exchange candidates and credentials, this will cause establish connectivity, be aware that as soon as connectivity is established (component state is CONNECTED), nicesrc may start producing data packets. Don't forget to monitor the state of each component.

At any time after the libnice Stream has been created, you can create `nicesink` elements and add them to your pipeline to send packets, you can do that at the same time as the `nicesrc` elements or later.
b
