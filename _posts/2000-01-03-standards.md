---
title: "standards"
bg: orange
color: black
fa-icon: sticky-note-o
---

libnice implements a number of current standards:

- [RFC 8445](https://tools.ietf.org/html/rfc8445): Interactive
  Connectivity Establishment (ICE) itself
- [RFC 5766](https://tools.ietf.org/html/rfc5766): TURN relay client
- [RFC 5389](https://tools.ietf.org/html/rfc5389): Full STUN implementation
- [RFC 6544](https://tools.ietf.org/html/rfc6544): TCP Candidates with ICE (ICE-TCP)
  - Partial support, only passive and active candidates are supported
- [Trickle ICE](https://tools.ietf.org/html/draft-ietf-ice-trickle): Incremental Provisioning of Candidates
- Compatible with IPv4 & IPv6

It also implements a number of older version with appropriate
compatibility flags:
- [RFC 5245](https://tools.ietf.org/html/rfc5245): The original ICE RFC
  - Also implements Draft 6 as used by older programs.
- [RFC 3489](https://tools.ietf.org/html/rfc3489): The original STUN RFC

There is also compatibility mode with non-standard implemenations:
- Microsoft's implementations, both compatible with Windows Live Messenger 2009, Office Communicator 2007, Office Communicator 2007R2, Lync, Skype for Business.  - Those are mostly documented in [MS-ICE2](https://interoperability.blob.core.windows.net/files/MS-ICE2/[MS-ICE2].pdf)
- Compatibility with the old variant used by Google Talk

